# cimbend-datamodel 

This repository hosts  the UML data model for the products of Zepben and associated projects including the [Evolve DERMS Framework](https://www.zepben.com/solutions/evolve).
The file is allocated in a [Sparx - Enterprise Architect version 15](https://sparxsystems.com/products/ea/15/index.html) native format (.eap).

If you do not have Enterprise Architect version 15, Sparx provides a free viewer (EALite) that is available for download at their website which can be read by the [EA lite edition](https://www.sparxsystems.eu/enterprise-architect/ea-lite-edition/).      

**cimben-datamodel** data model is a profile (sub-set of classes, attributes and realtionships) of the Common Information Data Model (CIM)  UML Model file: 
[iec61970cim17v38_iec61968cim13v13a_iec62325cim03v17a_CIM100.1.1.eap](https://cimug.ucaiug.org/CIM%20Model%20Releases/Forms/AllItems.aspx). 

# Mappings 

Additionally, in the directory */mappings/* the data model mapping to other data models and implementation are documented in google sheets files, including:

* *[cimproto-to-evolve2.0](https://docs.google.com/spreadsheets/d/1rO7lgf363MXr42VPdezTECpzrgN0iAtkWpiCRgUjglE/edit?usp=sharing)*: Mapping to the message formats (.proto files) of the Google protocol buffers implementation. 
* *[evolve2.0-to-endevour](https://docs.google.com/spreadsheets/d/1LNsBqa2bnSBKB5TOIJ5Q8cSi7E5odybWAfqUVul_TwE/edit?usp=sharing).*: Mapping to Endevour RDF/XML  payload. 
* *[evolve2.1-to-sincal15](https://docs.google.com/spreadsheets/d/1jivMxvccxxEAxivdfAQlgWUizsj1LphC8aM9bSpFwDw/edit?usp=sharing)*: Mapping to [SINCAL v15.0](https://pss-store.siemens.com/store?Action=list&Locale=en_IE&SiteID=sipti&ThemeID=4816868000&categoryID=4885313400&s_kwcid=AL!462!3!463152941275!e!!g!!sincal&ef_id=EAIaIQobChMIyp2Ls4Dt6wIVhyRgCh1ckQXYEAAYASAAEgJHdfD_BwE:G:s) Database Description. 
 
# Documentation 

Please check the [Evolve Data Model documentation website](https://zepben.bitbucket.io/cim/evolve/)  for more details. 


 
